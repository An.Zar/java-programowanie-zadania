package pl.sda;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        float circleDiameter = sc.nextFloat();
        final float pi = 3.14f;

        if (circleDiameter > 0) {
            float circumference = circleDiameter * pi;
            double circumference2 = circleDiameter * Math.PI;
            System.out.printf("%.3f%n", circumference);
            System.out.println("Obwód koła obliczany wykorzystując klasę Math: " + circumference2);
        }
    }
}



